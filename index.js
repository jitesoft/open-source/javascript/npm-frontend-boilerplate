#!/usr/bin/env node

const { Manager } = require('@jitesoft/cli');
//const DisplayDependencies = require('./Handlers/DisplayDependencies');
const CreateProject = require('./Handlers/CreateProject');


// defaults.author = process.env.USERNAME || process.env.USER;


const cli = new Manager("npx @jitesoft/cli", "Simple and swift project creation for JavaScript projects.");

// Register commands.
cli.register(new CreateProject());

cli.invoke().then(r => {
  if (r) {
    process.stdout.write(r);
  }
  process.exit(0);
}).catch(e => {
  process.stderr.write(e.message);
  process.exit(2);
});
