const { Command, Argument, Option } = require('@jitesoft/cli');
const fs = require('fs').promises;
const https = require('https');

class CreateProject extends Command {
  static #revision = '/d568159fb1a4ffc16224613674cd7aadd31f4498';
  static #eslintTest = '{\n' +
    '  "parser": "babel-eslint",\n' +
    '  "extends": "@jitesoft/eslint-config/jest"\n' +
    '}\n';
  static #eslintBase = '{\n' +
    '  "parser": "babel-eslint",\n' +
    '  "extends": "@jitesoft"\n' +
    '}\n';
  static #babelBase = '{\n' +
    '    "presets": [\n' +
    '        "@jitesoft/main"\n' +
    '    ]\n' +
    '}\n';

  constructor () {
    super('init', 'Initialize a new project.', {
      args: [
        new Argument("Project name", "Name of the project.", true, String)
      ],
        options: [
          new Option('license', 'License to use.', [], false, String, false),
          new Option('description', 'Description of project.', [], false, String, false),
          new Option('author', 'The package author name (include email with `<email@domain.tld>`.', [], false, String, false),
          new Option('default', 'Use default packages and values not provided via options.', ['d'], false),
          new Option('no-dir', 'Create project in current directory.', ['n'], false),
          new Option('latest', 'Force generator to use latest available templates instead of specific revision.', [], false, null, false)
      ]
    });
  }

  async handle (command, input, args, options) {
    // Fetch package file.
    let packageFile = {};
    if (options.get('latest', false)) {
      CreateProject.#revision = '';
    }

    try {
      packageFile = JSON.parse(await this._getFile(
        `https://gist.githubusercontent.com/JiteBot/fe9a847ef01239f86df00f337b8bc63e/raw${CreateProject.#revision}/_package.json`
      ));
    } catch (ex) {
      await input.output('Failed to download template file for package.json. Please check your internet connection!');
    }

    packageFile.author = process.env.USERNAME || process.env.USER;

    if (!options.get('default', false)) {
      await input.output('Please fill out the following values to complete the initialization:');
    }

    packageFile.name        = args.get('Project name').value;
    const valuesWanted = [
      { name: 'description', default: packageFile.description },
      { name: 'license', default: packageFile.license },
      { name: 'author', default: packageFile.author }
    ];

    for (const value of valuesWanted) {
      // Check if an option exist.
      if (options.has(value.name)) {
        packageFile[value.name] = options.get(value.name).value;
      } else if (options.get('default', false)) {
        packageFile[value.name] = value.default;
      }
      else {
        packageFile[value.name] = await input.questionOr(value.name, value.default);
      }
    }

    // If package is scope, remove scope name.
    const folderName = packageFile['name'].indexOf('/') !== -1 ? packageFile['name'].split('/')[1] : packageFile['name'];
    const path = options.has('no-dir') ? '' : `${folderName}/`;

    // File creations.
    try {
      await this._createDictionaries(path);
      await this._touchSource(path);
      await this._touchTest(path);
      await this._createGitIgnore(path);
      await this._createBabelConfig(path);
      await this._createEslintRules(path);
      await this._createPackageJson(path, packageFile);
      await this._createReadme(path, packageFile['name'], packageFile['description']);
      await this._createWebpackConfig(path, packageFile['name']);
      await this._createLicense(path, packageFile['license']);
      await this._createGitlabCiFile(path);

      await input.output('Project created successfully! Open dir in terminal, write "git init && npm i" and start building!\nGood luck!\n');
    } catch (e) {
      await input.output(e.message);
    }
  }

  /**
   *
   * @param {String} uri
   * @return {Promise<String>}
   * @private
   */
  async _getFile(uri) {
    return new Promise((resolve, reject) => {
      let file = '';
      https.get(uri, {
        headers: {
          'user-agent': 'NPM package generator.'
        }
      }, response => {
        response.on('end', () => resolve(file));
        response.on('data', (d) => {
          file += d
        });
        response.on('error', reject);
      });
    });
  }

  async _createGitlabCiFile (path) {
    const ciFile = await this._getFile(
      `https://gist.githubusercontent.com/JiteBot/fe9a847ef01239f86df00f337b8bc63e/raw${CreateProject.#revision}/_gitlab.yml`
    );
    await fs.writeFile(`${path}.gitlab-ci.yml`, ciFile);
  }

  async _createGitIgnore (path) {
    const ignore = await this._getFile('https://raw.githubusercontent.com/github/gitignore/master/Node.gitignore');
    await fs.writeFile(`${path}.gitignore`, ignore);
  }

  async _createLicense (path, license) {
    let licenses = await this._getFile('https://api.github.com/licenses');
    try {
      licenses = JSON.parse(licenses);
      const index = licenses.findIndex((o) => o.key.toLowerCase() === license.toLowerCase());
      if (index !== -1) {
        license = JSON.parse(await this._getFile(licenses[index].url));
        await fs.writeFile(`${path}LICENSE`, license.body);
      }
    } catch (e) {
      // Do nothing.
      console.error(e);
    }
  }

  async _createWebpackConfig (path) {
    const webpackTemplate = await this._getFile(
      `https://gist.githubusercontent.com/JiteBot/fe9a847ef01239f86df00f337b8bc63e/raw/${CreateProject.#revision}_webpack.js`
    );
    await fs.writeFile(`${path}webpack.config.js`, webpackTemplate);
  }

  async _createEslintRules (path) {
    await fs.writeFile(`${path}.eslintrc.json`, CreateProject.#eslintBase);
    await fs.writeFile(`${path}tests/.eslintrc.json`, CreateProject.#eslintTest);
  }

  async _createBabelConfig (path) {
    await fs.writeFile(`${path}.babelrc.json`, CreateProject.#babelBase);
  }

  async _touchSource (path) {
    await fs.mkdir(`${path}src`);
    await fs.writeFile(`${path}src/index.js`, '// Index file, your main entrypoint!');
  }

  async _touchTest (path) {
    await fs.mkdir(`${path}tests`);
  }

  async _createDictionaries (path) {
    await fs.mkdir(`${path}`);
  }

  async _createReadme (path, projectName, projectDescription) {
    await fs.writeFile(`${path}README.md`, `# ${projectName}  \n  \n${projectDescription}`);
  }

  async _createPackageJson (path, values) {
    await fs.writeFile(`${path}package.json`, JSON.stringify(values, null, 4));
  }
}

module.exports = CreateProject;
