const { Command, Option } = require('@jitesoft/cli');
const defaults = require('../templates/package.template.json');

class DisplayDependencies extends Command {
  constructor () {
    super('dependencies', 'Displays the default dependencies added to the pakcage.json by the generator.', {
      options: [
        new Option('no-dev', 'Hide devDependencies.', ['-p'], false, null, false)
      ]
    });
  }

  async handle (command, input, args, options) {
    let list = 'Dependencies:\n';
    for (let dep in defaults.dependencies) {
      list += `\t${dep}\n`
    }

    if (!options.has('no-dev')) {
      list += '\nDevDependencies:\n';
      for (let dep in defaults.devDependencies) {
        list += `\t${dep}\n`
      }
    }

    await input.output(list);
  }
}

module.exports = DisplayDependencies;
