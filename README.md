# Boilerplate

Simple JavaScript package boilerplate generator.

This package is mainly intended to be used by the Jitesoft org, while it's okay to use it by anyone that wishes to!  

## What it does:

Creates a new project with pre-filled files to get you started right away!  
The following configuration files will be added to the project on initialization:

  * .babelrc.json  
  * .eslintrc.json
  * webpack.config.js
  * .gitlab-ci.yml

It will also add your license, a gitignore file and set up a standard directory structure.

### Important note

When the initialization runs, the package will download some external files, which are used as templates, the following URI's are accessed:

  * https://raw.githubusercontent.com/github/gitignore/master/Node.gitignore
  * https://api.github.com/licenses
  * https://api.github.com/licenses/LICENSE
  * https://gist.githubusercontent.com/JiteBot/fe9a847ef01239f86df00f337b8bc63e/raw/VERSION/_package.json
  * https://gist.githubusercontent.com/JiteBot/fe9a847ef01239f86df00f337b8bc63e/raw/VERSION/_gitlab.yml
  * https://gist.githubusercontent.com/JiteBot/fe9a847ef01239f86df00f337b8bc63e/raw/VERSION/_webpack.js

Where `VERSION` is depending on the version of this package that is used and `LICENSE` depends on which license you decide to use.

I recommend that you double-check the URI's that the program accesses and also check all the files afterwards to make sure that nothing is
wrong or odd, as you should with everything a third party downloads!

## Commands

To create a new project, run:

```bash
npx @jitesoft/npm-boilerplate init <package-name>
```

And answer the questions.  

If you run the `help init` command you will be able to get information about all possible optional parameters that the
generator honours.

### License

```text
MIT License

Copyright (c) 2018 Jitesoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

